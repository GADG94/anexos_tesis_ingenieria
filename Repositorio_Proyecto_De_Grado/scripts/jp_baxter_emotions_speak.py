#!/usr/bin/python2

import numpy as np
import math
import sys
import rospy
import cv2
import cv_bridge
#import pygame
from sensor_msgs.msg import Image
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from jp_baxtertry1.srv import *
import pygame
import sys
sys.path.append('/home/z420/ros_ws/src/jp_baxtertry1/scripts')
import Matriz_sonidos
x2 = 0
y2 = 0
primera = 0
array = 0

def callback(x,y,z):
    global imagess,z_1,x_S,y_S
    x_S=x
    y_S=y
    infoz_service = rospy.ServiceProxy("infoz_service", infoz)
    imagess=emocion(x,y)
    z_1=z
    #rospy.sleep(1)
    nada()
def nada():
	infoz_service = rospy.ServiceProxy("infoz_service", infoz)
	while not rospy.is_shutdown():
	    global imagess,x_S,y_S
	    rospy.wait_for_service('infoz_service')
	    response = infoz_service.call(infozRequest())
	    print response
 	    if response.z==0:
		break

	    if len(imagess)>0:
    		for path in imagess:
			aaa=0
		#rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.linear.z)
		#rospy.Subscriber('chatter', Twist, callback2
		if response.z==1:
			mat,y_S1,x_S1=Matriz_sonidos.sonido(x_S,y_S)
			FRAMERATE = 30
			pygame.init()
			clock = pygame.time.Clock()
    			pygame.mixer.music.load(mat[y_S1][x_S1])
    			pygame.mixer.music.play()
    			while pygame.mixer.music.get_busy():
				send_image2(path)
				rospy.sleep(0.1)
				send_image3(path)
				rospy.sleep(0.1)
				send_image2(path)
				rospy.sleep(0.1)
				send_image(path)	
				rospy.sleep(0.1)
        			clock.tick(FRAMERATE)
			break
		if response.z==2:
			send_image4(path)
			rospy.sleep(0.1)
			send_image5(path)
			rospy.sleep(0.1)
			send_image4(path)
			rospy.sleep(0.1)
			send_image(path)	
			rospy.sleep(0.1)
			break
		if response.z==3:
			send_image2(path)
			rospy.sleep(0.1)
			send_image3(path)
			rospy.sleep(0.1)
			send_image2(path)
			rospy.sleep(0.1)
			send_image(path)	
			rospy.sleep(0.1)	   
def send_image(path):
    path = '/home/z420/ros_ws/src/baxter_examples/share/matriz1/'+path
    img = cv2.imread(path)
    newimage = cv2.resize(img,(1024,600))
    #print newimage
    msg = cv_bridge.CvBridge().cv2_to_imgmsg(newimage, encoding="bgr8")
    pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=0.5)
    pub.publish(msg)
    #rospy.sleep(1)

def send_image2(path):
    path = '/home/z420/ros_ws/src/baxter_examples/share/images/'+path
    img = cv2.imread(path)
    newimage = cv2.resize(img,(1024,600))
    #print newimage
    msg = cv_bridge.CvBridge().cv2_to_imgmsg(newimage, encoding="bgr8")
    pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=0.5)
    pub.publish(msg)

def send_image3(path):
    path = '/home/z420/ros_ws/src/baxter_examples/share/matriz3/'+path
    img = cv2.imread(path)
    newimage = cv2.resize(img,(1024,600))
    #print newimage
    msg = cv_bridge.CvBridge().cv2_to_imgmsg(newimage, encoding="bgr8")
    pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=0.5)
    pub.publish(msg)
def send_image4(path):
    path = '/home/z420/ros_ws/src/baxter_examples/share/matriz4/'+path
    img = cv2.imread(path)
    newimage = cv2.resize(img,(1024,600))
    #print newimage
    msg = cv_bridge.CvBridge().cv2_to_imgmsg(newimage, encoding="bgr8")
    pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=0.5)
    pub.publish(msg)
def send_image5(path):
    path = '/home/z420/ros_ws/src/baxter_examples/share/matriz5/'+path
    img = cv2.imread(path)
    newimage = cv2.resize(img,(1024,600))
    #print newimage
    msg = cv_bridge.CvBridge().cv2_to_imgmsg(newimage, encoding="bgr8")
    pub = rospy.Publisher('/robot/xdisplay', Image, latch=True, queue_size=0.5)
    pub.publish(msg)
def emocion(x,y):
    global x2
    global y2
    global primera
    global array
    if primera == 0:
    	x1 = 0
    	y1 = 0
	primera = 1
    else:
	array=0
	x1 = x2
	y1 = y2
    	x2 = x*20/400
    	y2 = y*20/400

    Espacio_Emociones = np.reshape(np.arange(400),(20,20))
    a=1
    for x in range (0, 20):
	for y in range (0, 20):
	    Espacio_Emociones[x][y] = a
	    a = a+1
    im=["" for x in range(1,401)]
    nombre=".png"

    while (x1 < x2) & (y1 < y2):
	if x1 < x2:
		x1 = x1+1
	if y1 < y2:
		y1 = y1+1
        im[array]=str(Espacio_Emociones[y1,x1])+nombre
        array=array+1

    while (x1 > x2) & (y1 > y2):
	if x1 > x2:
		x1 = x1-1
	if y1 > y2:
		y1 = y1-1
        im[array]=str(Espacio_Emociones[y1,x1])+nombre
        array=array+1

    while (x1 < x2) | (y1 > y2):
	if x1 < x2:
		x1 = x1+1
	if y1 > y2:
		y1 = y1-1
        im[array]=str(Espacio_Emociones[y1,x1])+nombre
        array=array+1

    while (x1 > x2) | (y1 < y2):
	if x1 > x2:
		x1 = x1-1
	if y1 < y2:
		y1 = y1+1
        im[array]=str(Espacio_Emociones[y1,x1])+nombre
        array=array+1

    imagess= ["" for x in range(0,array)]
    for x in range(array):
        imagess[x]=im[x]
    #rospy.init_node('rsdk_xdisplay_image', anonymous=True)
    for path in imagess:
        send_image(path)
	rospy.sleep(0.1)
    return imagess	
    #z_1=1
   # if len(imagess)>0 and z==1:
	#print z
	#send_image2(path)
	#rospy.sleep(0.1)
	#send_image3(path)
	#rospy.sleep(0.1)
	#send_image(path)	
	#rospy.sleep(0.1)
    	#global z_1
	#rospy.Subscriber('chatter', Twist, callback2)


